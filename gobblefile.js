var path = require( 'path' );
var gobble = require( 'gobble' );
var gia = require( './Giafile' );

var prod = gobble.env() === 'production';

var baseUrl = ( prod ? ( 'https://interactive.guim.co.uk/' + gia.path ) : '.' );
var version = 'v/' + ( true ? Date.now() : 'dev' );


module.exports = gobble([

	// preview
	gobble( 'preview' ).transform( 'replace', {
		baseUrl: baseUrl,
		fonts: baseUrl + '/resources/fonts.css'
	}),

	// boot.js
	gobble( 'src/boot.js' ).transform( 'replace', {
		baseUrl: baseUrl,
		version: version
	}),

	gobble( 'src/app' )
		.transform( 'rollup', {
			entry: 'app.js',
			dest: 'app.js',
			format: 'amd',
			plugins: [
				require( 'rollup-plugin-ractive' )(),
				require( 'rollup-plugin-node-resolve' )(),
				require( 'rollup-plugin-commonjs' )(),
				require( 'rollup-plugin-buble' )()
			]
		})
		.moveTo( path.join( version, 'js' ) ),

	// files
	gobble( 'resources' ).moveTo( 'resources' )

]);
