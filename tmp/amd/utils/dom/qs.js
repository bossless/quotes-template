define(function () {

	'use strict';
	
	return function qs ( selector ) {
		return document.querySelector( selector );
	};

});