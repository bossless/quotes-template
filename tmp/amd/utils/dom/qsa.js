define(function () {

	'use strict';
	
	return function qsa ( selector ) {
		return document.querySelectorAll( selector );
	};

});