define(['utils/core/Promise'],function (Promise) {

	'use strict';
	
	return function loadImage ( src ) {
		return new Promise( ( fulfil, reject ) => {
			var img = new Image();
	
			img.onload = function () {
				fulfil( img );
			};
	
			img.onerror = reject;
	
			img.src = src;
		});
	};

});