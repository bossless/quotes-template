define(['utils/compatibility/requestAnimationFrame'],function () {

	'use strict';
	
	return window.cancelAnimationFrame;

});