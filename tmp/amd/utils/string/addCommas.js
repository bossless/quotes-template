define(function () {

	'use strict';
	
	return function addCommas ( num ) {
		return num.toString().replace( /\B(?=(\d{3})+(?!\d))/g, ',' );
	};

});