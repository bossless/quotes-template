import BaseView from './BaseView.html';
import { get } from 'gia-toolkit';

var app = {
	launch: function ( el ) {
		// optionally, comment this out...
		const view = new BaseView({ el });

		//find key
		const urlKey = window.location.search;
		const urlSlice = urlKey.slice(1, urlKey.length);
		const key = urlSlice.split('=')[1];

		get( `https://interactive.guim.co.uk/spreadsheetdata/${key}.json` ).then( JSON.parse ).then( data => {
			view.set({
				quotes: data.sheets.Responses,
				top: data.sheets.Header
			});
		});
	}
};

export default app;
